/* 
 * File:   Renderable.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: interface renderable permettant de definir la fonction de base d'afficahge des bouncable
 */
package bounceApp.render;

import bounceApp.bouncer.Bouncable;
import java.awt.Graphics2D;

/**
 *
 * @author aurel
 */
public interface Renderable {

   void display(Graphics2D g, Bouncable b);
}
