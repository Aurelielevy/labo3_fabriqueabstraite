/* 
 * File:   StrockedRenderer.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: Classe des renderers vide
 */
package bounceApp.render;

import bounceApp.bouncer.Bouncable;
import java.awt.BasicStroke;
import java.awt.Graphics2D;

public class StrockedRenderer implements Renderable {

   private static StrockedRenderer instance;

   /**
    * recuperation de l'instance unique StrockedRenderer
    *
    * @return l'instance unique StrockedRenderer
    */
   public static StrockedRenderer getInstance() {
      if (instance == null) {
         instance = new StrockedRenderer();
      }

      return instance;
   }

   /**
    * constructeur prive afin de ne pas pouvoir le creer si on est utilisateur
    */
   private StrockedRenderer() {
   }

   /**
    * affichage de la forme
    *
    * @param g graphics2D
    * @param b forme
    */
   @Override
   public void display(Graphics2D g, Bouncable b) {
      g.setColor(b.getColor());
      g.setStroke(new BasicStroke(2));
      g.draw(b.getShape());
   }

}
