/* 
 * File:   FilledRenderer.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: Classe des renderers remplis
 */

package bounceApp.render;

import bounceApp.bouncer.Bouncable;
import java.awt.Graphics2D;

public class FilledRenderer implements Renderable {

   private static FilledRenderer instance;

   /**
    * recuperation de l'instance unique FilledRenderer
    * @return l'instance unique FilledRenderer
    */
   public static FilledRenderer getInstance() {
      if (instance == null) {
         instance = new FilledRenderer();
      }

      return instance;
   }

   /**
    * constructeur prive afin de ne pas pouvoir le creer si on est utilisateur
    */
   private FilledRenderer() {
   }

   /**
    * affichage de la forme
    * @param g graphics2D
    * @param b forme
    */
   @Override
   public void display(Graphics2D g, Bouncable b) {
      g.setColor(b.getColor());
      g.fill(b.getShape());
   }

}
