/* 
 * File:   Display.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17
 * Goal: Classe permettant les affichages
 */
package bounceApp;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyAdapter;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Display implements Displayer, ComponentListener {

   private final JFrame frameDisplay;
   private static Display instance;
   private final JPanel panel;
   private BufferedImage renderImage;
   private Graphics2D graphics;
   private Dimension size;

   /**
    * constructeur de la classe. Creation de la frame, de l'image interne de
    * fond ainsi que du panel
    */
   private Display() {
      frameDisplay = new JFrame();
      size = new Dimension(500, 500);
      frameDisplay.setSize(size);

      //image interne
      renderImage = new BufferedImage((int) size.getWidth(), (int) size.getHeight(), BufferedImage.TYPE_INT_ARGB);
      graphics = renderImage.createGraphics();
      graphics.setBackground(Color.WHITE);

      panel = new JPanel() {
         @Override
         public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(renderImage, 0, 0, this);
         }
      };

      frameDisplay.setContentPane(panel);
      frameDisplay.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frameDisplay.setVisible(true);

      panel.addComponentListener(this);

   }

   /**
    * methode permettant de recuperer l'instance singleton du display
    *
    * @return l'instance singleton du display
    */
   public static Display getInstance() {
      //creation unique de l'instance
      if (instance == null) {
         instance = new Display();
      }
      return instance;
   }

   /**
    * Recuperation de la largeur de frameDisplay
    *
    * @return la largeur de frameDisplay
    */
   @Override
   public int getWidth() {
      return frameDisplay.getContentPane().getWidth();
   }

   /**
    * Recuperation de la hauteur de frameDisplay
    *
    * @return la hauteur de frameDisplay
    */
   @Override
   public int getHeight() {
      return frameDisplay.getContentPane().getHeight();
   }

   /**
    * Recuperation du Graphics2D utilise pour l'affichage
    *
    * @return l'attribut graphics caste en Graphics2D
    */
   @Override
   public Graphics2D getGraphics() {
      return (Graphics2D) this.graphics;
   }

   /**
    * repaint l'affichage
    */
   @Override
   public void repaint() {
      frameDisplay.getContentPane().repaint();
   }

   /**
    * modification du titre du frameDisplay
    *
    * @param s
    */
   @Override
   public void setTitle(String s) {
      frameDisplay.setTitle(s);
   }

   /**
    * Ajoute un listener pour les touches de clavier
    *
    * @param ka evenement du clavier
    */
   @Override
   public void addKeyListener(KeyAdapter ka) {
      this.frameDisplay.addKeyListener(ka);
   }

   /**
    * redimensionnement des composants lors du redimensionnement de la fenetre
    *
    * @param ce Evenement indiquant le changement d'etat de la fenetre.
    * Inutilise dans notre cas
    */
   @Override
   public void componentResized(ComponentEvent ce) {
      renderImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
      graphics = renderImage.createGraphics();
      graphics.setBackground(Color.WHITE);
      size = panel.getSize();
      repaint();
   }

   /**
    * methode overridee ne faisant rien car inutile dans notre cas
    *
    * @param ce
    */
   @Override
   public void componentMoved(ComponentEvent ce) {
   }

   /**
    * methode overridee ne faisant rien car inutile dans notre cas
    *
    * @param ce
    */
   @Override
   public void componentShown(ComponentEvent ce) {
   }

   /**
    * methode overridee ne faisant rien car inutile dans notre cas
    *
    * @param ce
    */
   @Override
   public void componentHidden(ComponentEvent ce) {
   }

}
