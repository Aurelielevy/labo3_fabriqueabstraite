/* 
 * File:   Displayer.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: Interface definissant les fonctions necessaires pour l'afficheur
 */
package bounceApp;

import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;

public interface Displayer {

   int getWidth();

   int getHeight();

   Graphics2D getGraphics();

   void repaint();

   void setTitle(String s);

   void addKeyListener(KeyAdapter ka);
}
