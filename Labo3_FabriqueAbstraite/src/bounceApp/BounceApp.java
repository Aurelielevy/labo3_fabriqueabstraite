/* 
 * File:   BounceApp.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: Classe principale. Permet de gérer l'application
 */
package bounceApp;

import bounceApp.bouncer.Bouncable;
import bounceApp.bouncer.BouncerFactory;
import bounceApp.bouncer.FilledBouncerFactory;
import bounceApp.bouncer.StrockedBouncerFactory;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import javax.swing.Timer;

public class BounceApp {

    /**
     * Permet de supprimer tous les bouncers de l'écran
     */
    private void clean() {
        bouncers.clear();
        clearPage();
    }

    /**
     * Vide la fenêtre de toutes formes
     */
    private void clearPage() {
        Display display = Display.getInstance();
        //On clear toute la page
        display.getGraphics().clearRect(0, 0, display.getWidth(), display.getHeight());
    }

    private LinkedList<Bouncable> bouncers;
    // Autres attributs

    //creation de l'attribut timer permettant les mouvements des formes
    private Timer loopTimer = new Timer(10, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            Display display = Display.getInstance();
            //On clear toute la page
            clearPage();

            //On dessine tous les bouncable
            for (Bouncable b : bouncers) {
                b.move();
                b.draw();
            }
            display.repaint();

        }
    });

    /**
     * creation et ajouts des CircleBouncer et SquareBouncer
     *
     * @param factory fabrique abstraite
     */
    private void createBouncers(BouncerFactory factory) {
        for (int i = 0; i < 10; i++) {
            bouncers.add(factory.createCircleBouncer());
            bouncers.add(factory.createSquareBouncer());
        }
    }

    /**
     * Gestion de l'application, des touches utilisables et des effets qu'elles
     * ont sur ladite application
     */
    public BounceApp() {
        bouncers = new LinkedList<>();
        Display.getInstance().setTitle("Bouncers");

        Display.getInstance().addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent ke) {
                switch (ke.getKeyChar()) {
                    case 'b':
                        //creation des formes "creuses"
                        createBouncers(StrockedBouncerFactory.getInstance());
                        break;
                    case 'f':
                        //creation des formes "pleines"
                        createBouncers(FilledBouncerFactory.getInstance());
                        break;
                    case 'e':
                        //nettoyage de l'affichage
                        clean();
                        break;
                    case 'q':
                        //fermeture de l'application
                        System.exit(0);
                        break;
                }
            }

        });
    }

    /**
     * lancement de l'attribut timer pour les bouncable
     */
    public void loop() {
        loopTimer.start();
    }

    /**
     * main de l'application
     *
     * @param args
     */
    public static void main(String... args) {
        new BounceApp().loop();
    }
}
