/* 
 * File:   BounceApp.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: Classe des formes rebondissantes (bouncer)
 * Contient les fonctions de mouvements, de dessins des formes
 */
package bounceApp.bouncer;

import bounceApp.Display;
import java.util.Random;

public abstract class Bouncer implements Bouncable {

   //valeur random permettant le calcul des vitesses aleatoires
   private static Random random = new Random();

   /* Notes sur les vitesses et positions: 
    *    Il aurait été possible d'utiliser des vecteurs,
    *    Par simplification, nous avons préféré utiliser
    *    directement des attributs
    */
   
   //vitesse selon les axes
   private int xSpeed;
   private int ySpeed;

   //position selon les axes
   private int xPos;
   private int yPos;

   //taille
   private int size;

   /**
    * constructeur definissant la vitesse, la taille et la position de depart de
    * la forme
    */
   public Bouncer() {
      while (xSpeed == 0) {
         this.xSpeed = random.nextInt(10) - 5;
      }
      while (ySpeed == 0) {
         this.ySpeed = random.nextInt(10) - 5;
      }

      this.size = random.nextInt(150);

      this.xPos = (Display.getInstance().getWidth() - size) / 2;
      this.yPos = (Display.getInstance().getHeight() - size) / 2;

   }

   /**
    * recuperation de la position sur l'axe x
    *
    * @return la position sur l'axe x
    */
   protected int getXPos() {
      return xPos;
   }

   /**
    * recuperation de la position sur l'axe y
    *
    * @return la position sur l'axe y
    */
   protected int getYPos() {
      return yPos;
   }

   /**
    * recuperation de la taille de la forme
    *
    * @return
    */
   protected int getSize() {
      return size;
   }

   /**
    * dessin de la forme
    */
   @Override
   public void draw() {
      getRenderer().display(Display.getInstance().getGraphics(), this);
   }

   /**
    * mouvement de la forme selon sa position et sa vitesse. Rebond si
    * necessaire
    */
   @Override
   public void move() {
      xPos += xSpeed;
      yPos += ySpeed;

      //On replace à la bonne position le bouncer
      if (xPos < 0) {
         xPos = 0;
         xSpeed *= -1;
      }
      if (xPos + size >= Display.getInstance().getWidth()) {
         xPos = Display.getInstance().getWidth() - size;
         xSpeed *= -1;
      }

      if (yPos < 0) {
         yPos = 0;
         ySpeed *= -1;
      }

      if (yPos + size >= Display.getInstance().getHeight()) {
         yPos = Display.getInstance().getHeight() - size;
         ySpeed *= -1;
      }
   }
}
