/* 
 * File:   StrockedBouncerFactory.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: Classe de la fabrique des formes vides
 */
package bounceApp.bouncer;

public class StrockedBouncerFactory implements BouncerFactory {

   private static StrockedBouncerFactory instance;

   /**
    * recuperation de l'instance StrockedBouncerFactory. Unique
    *
    * @return l'unique instance StrockedBouncerFactory
    */
   public static StrockedBouncerFactory getInstance() {
      if (instance == null) {
         instance = new StrockedBouncerFactory();
      }

      return instance;
   }
   
   
   //Constructeur privé : singleton
   private StrockedBouncerFactory(){}

   /**
    * Creation d'un cercle vide
    *
    * @return cercle vide
    */
   @Override
   public CircleBouncer createCircleBouncer() {
      return new StrockedCircleBouncer();
   }

   /**
    * creation d'un carre vide
    *
    * @return carre vide
    */
   @Override
   public SquareBouncer createSquareBouncer() {
      return new StrockedSquareBouncer();
   }

}
