/* 
 * File:   CircleBouncer.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: Classe des cercles "rebondissants"
 */
package bounceApp.bouncer;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;

public abstract class CircleBouncer extends Bouncer {

   /**
    * Recuperation de la forme definissant cette classe: un cercle
    *
    * @return une forme de cercle
    */
   @Override
   public Shape getShape() {
      return new Ellipse2D.Double(getXPos(), getYPos(), getSize(), getSize());
   }
}
