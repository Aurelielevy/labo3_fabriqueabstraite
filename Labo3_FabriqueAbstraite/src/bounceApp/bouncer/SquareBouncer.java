/* 
 * File:   SquareBouncer.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: Classe des carres "rebondissants"
 */
package bounceApp.bouncer;

import java.awt.Shape;
import java.awt.geom.Rectangle2D;

public abstract class SquareBouncer extends Bouncer {

   /**
    * Recuperation de la forme definissant cette classe: un cercle
    *
    * @return une forme de cercle
    */
   @Override
   public Shape getShape() {
      return new Rectangle2D.Double(getXPos(), getYPos(), getSize(), getSize());
   }

}
