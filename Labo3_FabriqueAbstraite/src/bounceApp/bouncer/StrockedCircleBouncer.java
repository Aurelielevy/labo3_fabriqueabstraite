/* 
 * File:   StrockedCircleBouncer.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: Classe des cercles vides
 */
package bounceApp.bouncer;

import bounceApp.render.*;
import java.awt.Color;

public class StrockedCircleBouncer extends CircleBouncer {

   /**
    * recupere l'instance de la forme cercle vide
    *
    * @return forme cercle vide
    */
   @Override
   public Renderable getRenderer() {
      return StrockedRenderer.getInstance();
   }

   /**
    * recupere la couleur du cercle. Ici: vert
    *
    * @return couleur vert
    */
   @Override
   public Color getColor() {
      return Color.GREEN;
   }

}
