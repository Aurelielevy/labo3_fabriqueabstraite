/* 
 * File:   FilledBouncerFactory.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: Classe de la fabrique des formes remplies
 */
package bounceApp.bouncer;

public class FilledBouncerFactory implements BouncerFactory {

   private static FilledBouncerFactory instance;

   /**
    * recuperation de l'instance FilledBouncerFactory. Unique
    *
    * @return l'unique instance FilledBouncerFactory
    */
   public static FilledBouncerFactory getInstance() {
      if (instance == null) {
         instance = new FilledBouncerFactory();
      }

      return instance;
   }
   
   
   //Constructeur privé : singleton
   private FilledBouncerFactory(){}

   /**
    * Creation d'un cercle rempli
    *
    * @return cercle rempli
    */
   @Override
   public CircleBouncer createCircleBouncer() {
      return new FilledCircleBouncer();
   }

   /**
    * creation d'un carre rempli
    *
    * @return carre rempli
    */
   @Override
   public SquareBouncer createSquareBouncer() {
      return new FilledSquareBouncer();
   }

}
