/* 
 * File:   BouncerFactory.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: Interface pour la fabrique des bouncers
 * Contient les fonctions de creation des formes
 */
package bounceApp.bouncer;

public interface BouncerFactory {

   public CircleBouncer createCircleBouncer();

   public SquareBouncer createSquareBouncer();
}
