/* 
 * File:   StrockedSquareBouncer.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: Classe des carres vides
 */
package bounceApp.bouncer;

import bounceApp.render.*;
import java.awt.Color;

public class StrockedSquareBouncer extends SquareBouncer {

   /**
    * recupere l'instance de la forme carre vide
    *
    * @return forme carre vide
    */
   @Override
   public Renderable getRenderer() {
      return StrockedRenderer.getInstance();
   }

   /**
    * recupere la couleur du carre. Ici: rouge
    *
    * @return couleur rouge
    */
   @Override
   public Color getColor() {
      return Color.RED;
   }

}
