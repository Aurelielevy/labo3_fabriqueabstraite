/* 
 * File:   Bouncable.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: Interface definissant les fonctions pour les rebonds
 * Ainsi que les formes (couleurs etc.)
 */
package bounceApp.bouncer;

import bounceApp.render.Renderable;
import java.awt.Color;
import java.awt.Shape;

public interface Bouncable {

   void draw();

   void move();

   Renderable getRenderer();

   Color getColor();

   Shape getShape();
}
