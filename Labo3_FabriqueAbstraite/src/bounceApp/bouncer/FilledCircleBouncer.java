/* 
 * File:   FilledCircleBouncer.java
 * Author: Remi Jacquemard, Aurelie Levy
 * Created on 03.17 - 04.17
 * Goal: Classe des cercles remplis
 */
package bounceApp.bouncer;

import bounceApp.render.*;
import java.awt.Color;

public class FilledCircleBouncer extends CircleBouncer {

   /**
    * recupere l'instance de la forme cercle remplie
    *
    * @return forme cercle remplie
    */
   @Override
   public Renderable getRenderer() {
      return FilledRenderer.getInstance();
   }

   /**
    * recupere la couleur du cercle. Ici: bleu
    *
    * @return couleur bleue
    */
   @Override
   public Color getColor() {
      return Color.BLUE;
   }

}
